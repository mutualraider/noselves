// Import VAW Prison Timeline (csv to json)
// =============================================================================
// Setup:  `npm install`
// Run:    `node scripts/import-csv-files/import-vaw-prison-timeline-csv.js`
// Input:  `input/vaw-prison-timeline.csv`
// Output: `build/vaw-prison-timeline.json`
//
// Please do NOT circumvent the `.gitignore` files and commit the input or
// output of this script.  Files in `input/` and `build/` may contain
// in-progress changes or personal information that's not ready to be released
// yet. :)
const csv = require('csvtojson');
const fs = require('fs');

const inputFilePath = 'input/vaw-prison-timeline.csv';
const outputFilePath = 'build/vaw-prison-timeline.json';

const timelineEntries = [];

console.log('==================== START ====================');

csv()
  .fromFile(inputFilePath)
  .on('json', (jsonObj) => {
    timelineEntries.push(jsonObj);
  })
  .on('done', (error) => {
    if (error !== undefined) {
      console.log('---------- ERROR ----------');
      console.log(error);
    }
    else {
      console.log('Writing file...');
      fs.writeFileSync(outputFilePath, JSON.stringify(timelineEntries));
      console.log('Done!');
    }

    console.log('==================== END ====================');
  });
