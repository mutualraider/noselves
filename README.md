**NOTE:** A lot of this is very preliminary and will likely be changed / added to moving forward.

## Project:

An online course focused on understanding the criminalization of survivors of violence and how to support them.

## Goals:

[@prisonculture](https://twitter.com/prisonculture) and the [Survived & Punished](http://www.survivedandpunished.org/) crew provide IRL trainings for free, which requires their unpaid labor and limits the number of trainings they can hold.  This would replace or supplement those IRL trainings to both reduce the amount of unpaid labor required and increase the number of people who are educated on these issues.

Specifically, we want to:

  - Create accessible and engaging in-depth educational content.
  - Create administrative tools that are used to keep the content up to date.
  - Provide direction and support materials for people who want to get involved, and encourage real-world / real-impact action (jail support groups, not Facebook likes).
  - Increase exposure and save @prisonculture and the Survived & Punished crew time.

## Requirements:

  - Minimize production and operating costs since this is all done on a shoestring budget.
  - Follow accessibility standards (for screen readers, etc).
  - Build in translation capabilities to offer content in both English and Spanish.

## Timeline:

**NOTE:** Timeline subject to change.  Currently only highlighting the more immediate steps, and the larger milestones.

Ideally we can keep a low pressure weekly cadance that works towards larger monthly milestones.  Take our time and focus on quality; lets not rush out bad work or burn people out.

  - **February:**
      * Setup the project.
      * Setup the tech.
      * Begin sourcing and processing content (translation, digitization, etc).
      * Begin designing the site and building out the skeleton.
      * Begin building interactive content (in isolated modules that can be
        integrated into the site later on).

  - **March & April:** Build build build.

  - **May:**
      * Review a rough draft.
      * Do some manual testing: Accurate translations?  Easy to use with a screen reader?  Engaging interactions?  etc

  - **June:** Refine refine refine.

  - **July:** Launch!  Pop the champaign bottles and break out the Hennessy.

  - **August and onwards:** Fix fix fix all the leaky pipes that people discover.

## Educational Modules:

[@prisonculture](https://twitter.com/prisonculture) is coordinating the educational content portion of this project.

**NOTE:** This section is likely to change A LOT in the coming weeks.

**Audience:** People who already have a basic understanding of the issues and are motivated to give themselves and in-depth education.  The [Survived & Punished](http://www.survivedandpunished.org/) website already covers a more cursory introduction to the topic.

 1. Introduction
      - Overview
      - History
      - Related / Intersectional Issues
      - Common Background and Terminology

 2. Surviving the Violence

 3. Arrest and Trial

 4. In Jail

 5. Coming Home

 6. Conclusion
      - Putting It All Together
      - Glossary
      - Related Links / Further Reading

 7. How to Help
      - Each module will include a "how to help" section that drives community engagement and direct action (jail support groups, not Facebook likes).  These sections will focus on the module that it is in, however all of them will also be pulled out into their own module at the end so they can be viewed with some continuity.

## Content:

**NOTE:** More content sources will be added in the coming weeks, and we'll begin linking the content to specific modules and features.

**Sources:**

  - [Survived & Punished](http://www.survivedandpunished.org/) and specifically the [S&P Toolkit](http://www.survivedandpunished.org/sp-toolkit.html).

**TODO:**
  - A small video introducing criminalized survival, Survived & Punished, and this site.
  - An interactive timeline.
  - Visualizations and interactive content.
  - Organize, edit, and markup existing content.

## Tech Considerations

[@mutualraider](https://twitter.com/mutualraider) is coordinating the tech portion of this project.

There is a **small** budget for hosting and to bring in outside resources (**ie a videographer**).  Lets use it wisely and be as resourceful as we can.

**TODO:**

  - Get a domain name. **[@prisonculture is on this]**
  - Choose a webstack.
      * Ensure it'll be easy to build an admin section with content editing / creation tools.
      * Ensure it'll be easy to build in localization capabilities to provide a spanish language translation.
  - Setup a local development environment, and get hosting for test / production environments.
  - Setup code quality guidelines, automated code linting, and automated testing.
      * Link to accessibility standards and best practices.  Try to automatically detect accessibility issues.
  - Create the damn thing already! :D
